# User scripts

This repo contains some [Greasemonkey](http://www.greasespot.net/) user scripts.

* **diaspora_hide_reshares.user.js**: Hides all posts in the Diaspora* stream, which are reposts
* **reddit_hide_all.user.js**: Automatically clicks all the hide buttons on the current [Reddit](https://www.reddit.com/) page

