// ==UserScript==
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// ATTENTION: This script stopped working with Diaspora v0.6
//            It is NOT MAINTAINED anymore.
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// @author      Strubbl
// @name        Diaspora* hide reshares
// @description hides all posts which are reshares, but shows summary for reshares with comments while hiding the reshare itself
// @include     https://pod.geraspora.de/stream
// @version     1.2
// @grant       none
// @updateURL   https://codeberg.org/strubbl/userscripts/raw/commit/c57073ffad428516100995e40475f5034e7dbb33/diaspora_hide_reshares.user.js
// ==/UserScript==

$(document).ready(function() {
  console.log('GreaseMonkey: Diaspora* hide reshares');
  var ms = $('#main_stream');
  var msHtml = ms.html();
  var msMonitoring = setInterval(function() {
    var newMsHtml = ms.html();
    if(msHtml != newMsHtml) {
      hideReshares();
      // do not use newMsHtml cause we are inserting html in hideReshares()
      msHtml = ms.html();
    }
  }, 1500);

  function hideReshares() {
    //console.log('hideReshares()');
    resharePost = $('div.stream_element:has(div.reshare)').each(function(index, stream_element) {
      //console.log('resharePost no. ' + index);
      var se = $(stream_element);
      var seTotalComments = 0;
      if(se.find('.reshare_summary').length > 0) {
        // prevent inserting multiple reshare summaries
        //console.log('has already a reshare summary');
        return;
      }
      var comment_stream = se.find('div.comment_stream');
      // TODO test for comment_stream.length > 0
      if(comment_stream.children('div.show_comments.comment.hidden').length === 0){
        // so we have some hidden comments for this stream element 
        // (cause there are too many comments)
        // fiddel the number of hidden comments out
        var hiddenCommentsInfo = comment_stream.find('a.toggle_post_comments');
        //console.log('hiddenCommentsInfo.html()=' + hiddenCommentsInfo.html());
        //console.log('hiddenCommentsInfo=' + JSON.stringify(hiddenCommentsInfo));
        //console.log('hiddenCommentsInfo.html()=' + hiddenCommentsInfo.html());
        var re = /(\d)/m;
        var matches = hiddenCommentsInfo.html().match(re);
        // TODO test matches for null
        //console.log('matches=' + matches);
        if(matches) {
          //console.log('matches[0]=' + matches[0]);
          // TODO test matches[0] for type number
          seTotalComments += parseInt(matches[0]);
          //console.log('seTotalComments after regex ' + seTotalComments);
        }
        else {
          // we expect there is only one comment, cause the regex failed
          // (cause they write "one" instead of 1)
          seTotalComments++;
        }
      } // else stream element has no hidden comments

      var comments = comment_stream.children('div.comments').find('div.comment');
      //console.log('comments=' + JSON.stringify(comments));
      //console.log('comments.length=' + comments.length);
      if(comments.length > 0) {
        seTotalComments += comments.length;
        var reshareLink = se.find('span.details.grey').first().html();
        var reshareAuthor = se.find('a.author.hovercardable').first().html();
        //console.log('reshareAuthor=' + reshareAuthor);
        var originalAuthor = se.find('div.reshare').find('a.author').first().html();
        //console.log('originalAuthor=' + originalAuthor);
        var media_div = se.children('div.media');
        var commentOrComments = 'comments';
        if(seTotalComments === 1) {
          commentOrComments = 'comment';
        }
        media_div.before('<div class="reshare_summary" style="padding: 10pt">Hidden Reshare by ' 
                         + reshareAuthor + ' from ' + originalAuthor + ' with ' 
                         + seTotalComments + ' ' + commentOrComments + ': ' + reshareLink + '</div>');
        // visual changes:
        //comments.css( 'border', '3px solid red' );
        //se.css( 'border', '3px solid red' );
        media_div.hide();
      }
      else {
        //se.css( 'border', '3px solid green' );
        se.hide();
      }
    }); // for each resharePost
  } // hideReshares() end
}); // document ready

