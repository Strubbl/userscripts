// ==UserScript==
// @author        Strubbl
// @author        miche101
// @name          OpenStreetMap Changeset Analyze URLs
// @description   adds links to achavi and OSMCha to every OSM changeset page
// @match         https://www.openstreetmap.org/changeset/*
// @grant         none
// @version       1.4
// @updateURL     https://codeberg.org/strubbl/userscripts/raw/branch/master/osm_changeset_analyze_urls.user.js
// ==/UserScript==

analyze_link_id = "osm_changeset_analyze_urls"

loadChangesetLinks()
setInterval(function(){
  if (!document.getElementById(analyze_link_id)) {
    loadChangesetLinks()
  }
}, 1000)

function loadChangesetLinks(){
  var href = window.location.href
  console.log("href: " + href)
  analyze_names = ["achavi", "achavi (dev)", "OSMCha"]
  analyze_urls = ["https://overpass-api.de/achavi/?changeset=", "https://dev.overpass-api.de/achavi/?changeset=", "https://osmcha.org/changesets/"]
  var cs_headline = document.getElementById("sidebar_content").getElementsByTagName('h2')[0]
  var cs_id = ""
  var i;
  var input_fields = document.getElementsByTagName('input')
  var stop_execution = false

  // get changeset id
  for (i = 0; i < input_fields.length; i++) {
    var check_for_cs_id = input_fields[i].getAttribute("data-changeset-id")
    if(check_for_cs_id) {
      cs_id = check_for_cs_id
      break
    }
  }

  // copied from post of miche101 https://forum.openstreetmap.org/viewtopic.php?pid=825651#p825651
  // https://www.openstreetmap.org/changeset/56501044#map=16/48.1818/11.7873
  if(!cs_id) {
     let regex = /.*changeset\//g;
     let result = href.replace (regex,"");
     let regexx = /#.*/g;
     let resultx = result.replace (regexx,"");
     cs_id = resultx;
  }

  console.log(cs_id)
  if(cs_id === "" || isNaN(cs_id) || Number.isNaN(cs_id)) {
    console.log("osm_changeset_analyze_urls: no changeset id found, cs_id=" + cs_id)
    stop_execution = true
  }

  if(analyze_names.length != analyze_urls.length) {
    console.log("osm_changeset_analyze_urls: analyze_names and analyze_urls have different length")
    stop_execution = true
  }

  if(!stop_execution) {
    // generate links html code
    var links_html = ""
    for (i = 0; i < analyze_names.length; i++) {
      links_html += "<a href=\"" + analyze_urls[i] + cs_id + "\">" + analyze_names[i] + "</a> "
    }
    // append links to website
    cs_headline.insertAdjacentHTML('afterend', "<div id=\"" + analyze_link_id + "\" style=\"color: #f00; padding-left: 20px;\">" + links_html + "</div>")
  }
}

