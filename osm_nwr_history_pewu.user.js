// ==UserScript==
// @author       miche101
// @author       Strubbl
// @name         OpenStreetMap History Viewer Link
// @description  adds link to OSM History Viewer (by PeWu) for every node, way or relation page
// @match        https://www.openstreetmap.org/node/*
// @match        https://www.openstreetmap.org/way/*
// @match        https://www.openstreetmap.org/relation/*
// @grant        none
// @updateURL    https://codeberg.org/strubbl/userscripts/raw/branch/master/osm_nwr_history_pewu.user.js
// @version      1.8
// ==/UserScript==

var history_link_id = "osm_nwr_hist_viewer_urls"

loadHistoryLinks()
setInterval(function(){
  if (!document.getElementById(history_link_id)) {
    loadHistoryLinks()
  }
}, 1000)

function loadHistoryLinks(){
  var hrefstr = window.location.href
  console.log("hrefstr: " + hrefstr)
  var n = hrefstr.search("/node/")
  var w = hrefstr.search("/way/")
  var r = hrefstr.search("/relation/")
  var stop_execution = false
  var object_id = ""
  if (n > 0) {
    console.log("node");
    osmobj="node";
  }
  else if (w > 0) {
    console.log("way");
    osmobj="way";
  }
  else if (r > 0) {
    console.log("relation");
    osmobj="relation";
  }
  else {
    console.error("did not find node, way or relation in URL")
    stop_execution = true
  }

  if(!stop_execution) {
    hist_viewer_names = ["History-PeWu"]
    // %OBJ% == node, way, relation   %ID% == OSM-ID
    hist_viewer_urls = ["https://pewu.github.io/osm-history/#/%OBJ%/%ID%"]

    // Link Example OSM History Viewer
    // https://pewu.github.io/osm-history/#/node/54154220
    // https://pewu.github.io/osm-history/#/way/683125725
    // https://pewu.github.io/osm-history/#/relation/9767454

    console.log("OSM-object with type: " + osmobj);

    let regex1 = /http.*openstreetmap\.org\/(node|way|relation)\//
    let result1 = hrefstr.replace(regex1,"")
    //console.log("result1: " + result1)
    let regex2 = /(\/history)?(#map=[0-9\.\/]+)?/g
    let result2 = result1.replace(regex2,"")
    object_id = result2

    console.log("OSM object with ID: " + object_id)

    if(!Number.isNaN(object_id) && object_id !== "") {
      // generate links html code
      var links_html = ""
      for (i = 0; i < hist_viewer_names.length; i++) {
        var url = hist_viewer_urls[i];
        var url2 = url.replace("%OBJ%", osmobj)
        var url3 = url2.replace("%ID%", object_id)
        links_html += "<a href=\"" + url3 + "\" target=\"_blank\">" + hist_viewer_names[i] + "</a> "
      }
      // append links to website
      var object_headline = document.getElementById("sidebar_content").getElementsByTagName('h2')[0]
      object_headline.insertAdjacentHTML('afterend', "<div id=\"" + history_link_id + "\" style=\"color: #f00; padding-left: 20px;\">" + links_html + "</div>")
    }
    else {
      console.error("found object_id is not a number: " + object_id)
    }
  }
}

