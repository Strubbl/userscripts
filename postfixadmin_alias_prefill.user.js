// ==UserScript==
// @author        Strubbl
// @name          Postfixadmin Alias prefill
// @description   Prefill the recipients textarea with a default value
// @include       http*://*/postfixadmin/public/edit.php?table=alias
// @grant         none
// @version       1.3
// @updateURL     https://codeberg.org/strubbl/userscripts/raw/branch/master/postfixadmin_alias_prefill.user.js
// ==/UserScript==

var prefill_aliases = "address@domain.tld"
document.forms['edit_alias'].elements['value[goto]'].value = prefill_aliases;
document.forms['edit_alias'].elements['value[localpart]'].focus();

