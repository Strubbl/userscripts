// ==UserScript==
// @author        original: Zxw, Firefox adjustments: Strubbl
// @name          Reddit Hide
// @description   adds to Reddit a Hide All button
// @include       http*://*.reddit.com/*
// @grant         none
// @version       1.5
// @updateURL     https://codeberg.org/strubbl/userscripts/raw/branch/master/reddit_hide_all.user.js
// ==/UserScript==

var hide_buttons = null;
var hide_button_index = null;

function pausecomp(millis) {
    var date = new Date();var curDate = null;do { curDate = new Date(); } while(curDate-date < millis);
}

function ready(callback){
  if (document.readyState!='loading') callback();
  else if (document.addEventListener) document.addEventListener('DOMContentLoaded', callback);
  else document.attachEvent('onreadystatechange', function(){
    if (document.readyState=='complete') callback();
  });
}

ready(function(){
  var tm = document.getElementsByClassName("tabmenu");
  for(var i = 0; i < tm.length; i++) {
    tm[i].innerHTML += '<li></li>';
    tm[i].innerHTML += '<li><a id="strubbl_copy" href="#">copy links</a></li>';
    tm[i].innerHTML += '<li><a id="zxw_hide" href="#">hide all</a></li>';
  }

  document.getElementById("strubbl_copy").onclick = function() {
    console.log("ready copy start");
    var title_links = document.getElementsByClassName("title");
    var all_links_text = ""
    for(var i = 0; i < title_links.length; i++) {
      if(title_links[i].href != undefined) {
      	all_links_text += title_links[i].href + "\n";
      }
    }
		copyTextToClipboard(all_links_text);
  }
  
  document.getElementById("zxw_hide").onclick = function() {
    hide_forms = document.getElementsByClassName("hide-button");
    hide_buttons = [];
    for(var i = 0; i < hide_forms.length; i++) {
      hide_buttons.push(hide_forms[i].getElementsByTagName("a")[0]);
    }
    hide_button_index = 0;
    f();
  }
});

function f() {
  if (hide_button_index < hide_buttons.length) {
    var hide_button = hide_buttons[hide_button_index];
    hide_button_index += 1;
    hide_button.click();
    setTimeout(f, 500);
  } else {
    pausecomp(300)
    window.location.reload();
  }
}


function copyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;
  textArea.style.width = '2em';
  textArea.style.height = '2em';
  textArea.style.padding = 0;
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';
  textArea.style.background = 'transparent';

  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.select();
  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }
  document.body.removeChild(textArea);
}

